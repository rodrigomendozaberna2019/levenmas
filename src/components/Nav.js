import React from 'react';
import {Link} from 'gatsby';

export default function Nav({
                                onClose = () => {
                                }
                            }) {
    return (
        <nav id="menu">
            <div className="inner">
                <h2>Menu</h2>
                <ul>
                    <li>
                        <Link to="/">Home</Link>
                    </li>
                    <li>
                        <Link to="/solution">Solución</Link>
                    </li>
                    <li>
                        <Link to="/products">Productos</Link>
                    </li>
                    <li>
                        <Link to="/about">Sobre nosotros</Link>
                    </li>
                    <li>
                        <Link to="/contact">Contacto</Link>
                    </li>
                    <li>
                        <Link to="/faq">Faq</Link>
                    </li>
                </ul>
            </div>
            <a
                className="close"
                onClick={e => {
                    e.preventDefault();
                    onClose();
                }}
                href="#menu"
            >
                Close
            </a>
        </nav>
    );
}
