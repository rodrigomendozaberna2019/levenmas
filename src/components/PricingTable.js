import React, {Fragment, useState} from "react";
import logo from '../assets/img/Logo.png'

function PricingTable() {
    const [one, setOne] = useState(false);
    const [two, setTwo] = useState(false);
    const [three, setThree] = useState(false);
    const [four, setFour] = useState(false);
    const [five, setFive] = useState(false);
    const [six, setSix] = useState(false);
    const [seven, setSeven] = useState(false);
    const [eight, setEight] = useState(false);
    const [nine, setNine] = useState(false);

    return (
        <div>
            <div className="">
                <div className="inner">
                    <section className="gh-pricing-table-wrapper">
                        <table className="gh-pricing-table u-background--white">
                            <tbody>
                            <tr className="gh-pricing-table-noborder">
                                <td className="price"><h2><span className="badge badge-primary">Detalles</span></h2>
                                </td>
                                <td className="price"><h2><span className="badge badge-primary">Básico</span></h2>
                                    <h3 className="gh-price">
                                        <div className="signo">$</div>
                                        <div className="precio">12,000.00 mxn</div>
                                    </h3>
                                </td>
                                <td className="price"><h2>Estandar</h2>
                                    <h3 className="gh-price">
                                        <div className="signo">$</div>
                                        <div className="precio">15,000.00 mxn</div>
                                    </h3>
                                </td>
                                <td className="price"><h2>Especial</h2>
                                    <h3 className="gh-price">
                                        <div className="signo">$</div>
                                        <div className="precio">15,000.00 mxn</div>
                                    </h3>
                                </td>
                                <td className="price"><h2>Premium</h2>
                                    <h3 className="gh-price">
                                        <div className="signo">$</div>
                                        <div className="precio">15,000.00 mxn</div>
                                    </h3>
                                </td>
                            </tr>
                            <tr className="gh-pricing-category" onClick={() => setOne(!one)} style={{cursor: 'pointer'}}>
                                <td colSpan="4">Registro de la contabilidad</td>
                                <td style={{border: 0}}>
                                    <img src="https://img.icons8.com/material-sharp/24/000000/sort-down.png" style={{width: 15, height: 10}}/>
                                </td>
                            </tr>
                            <tr style={{ display: (one ? "" : "none") }}>
                                <td className="subtitulo_tabla">Calendario mensual de compromisos</td>
                                <td><span className="hidden">Included when self-hosting</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path
                                                d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path
                                                d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path
                                                d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path
                                                d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (one ? "" : "none") }}>
                                <td className="subtitulo_tabla">Control de Documentos de contabilidad</td>
                                <td><span className="hidden">Included when self-hosting</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path
                                                d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path
                                                d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path
                                                d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path
                                                d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (one ? "" : "none") }}>
                                <td className="subtitulo_tabla">Control de pendientes</td>
                                <td><span className="hidden">Included when self-hosting</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path
                                                d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path
                                                d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path
                                                d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path
                                                d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (one ? "" : "none") }}>
                                <td className="subtitulo_tabla">Contabilidad de operaciones nacionales</td>
                                <td><span className="hidden">Included when self-hosting</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path
                                                d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path
                                                d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path
                                                d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path
                                                d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (one ? "" : "none") }}>
                                <td className="subtitulo_tabla">Contabilidad de operaraciones con el
                                    extrajero
                                </td>
                                <td><span className="hidden">Included when self-hosting</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path
                                                d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path
                                                d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path
                                                d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path
                                                d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (one ? "" : "none") }}>
                                <td className="subtitulo_tabla">Cierre contable</td>
                                <td><span className="hidden">Included when self-hosting</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path
                                                d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path
                                                d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path
                                                d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path
                                                d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (one ? "" : "none") }}>
                                <td className="subtitulo_tabla">Auditoria interna de las pólizas de
                                    contabilidad
                                </td>
                                <td><span className="hidden">Included when self-hosting</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path
                                                d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path
                                                d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path
                                                d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path
                                                d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr className="gh-pricing-category" onClick={() => setTwo(!two)} style={{cursor: 'pointer'}}>
                                <td colSpan="4">Calculo de impuestos</td>
                                <td style={{border: 0}}>
                                    <img src="https://img.icons8.com/material-sharp/24/000000/sort-down.png" style={{width: 15, height: 10}}/>
                                </td>
                            </tr>
                            <tr style={{ display: (two ? "" : "none") }}>
                                <td className="subtitulo_tabla">Cruce de reportes</td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (two ? "" : "none") }}>
                                <td className="subtitulo_tabla">Cálculo de impuestos del SAT</td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (two ? "" : "none") }}>
                                <td className="subtitulo_tabla">Revisión y autorización del cierre fiscal e impuestos
                                    mensuales
                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (two ? "" : "none") }}>
                                <td className="subtitulo_tabla">Envío de impuestos mensuales antes del día 12</td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (two ? "" : "none") }}>
                                <td className="subtitulo_tabla">Auditoria interna del cierre fiscal</td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr className="gh-pricing-category" onClick={() => setThree(!three)} style={{cursor: 'pointer'}}>
                                <td colSpan="4">Cumplimiento de obligaciones fiscales y legales</td>
                                <td style={{border: 0}}>
                                    <img src="https://img.icons8.com/material-sharp/24/000000/sort-down.png" style={{width: 15, height: 10}}/>
                                </td>
                            </tr>
                            <tr style={{ display: (three ? "" : "none") }}>
                                <td className="subtitulo_tabla">Prestación de las declaraciones en el portal del SAT
                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (three ? "" : "none") }}>
                                <td className="subtitulo_tabla">DIOT con datos</td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (three ? "" : "none") }}>
                                <td className="subtitulo_tabla">Contabilidad electronica</td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (three ? "" : "none") }}>
                                <td className="subtitulo_tabla">Observaciones y recomendaciones fiscales</td>
                                <td><span className="hidden">Included when self-hosting</span>

                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (three ? "" : "none") }}>
                                <td className="subtitulo_tabla">Elaboración de CFDI's por pago de proveedores
                                    extranjeros
                                </td>
                                <td><span className="hidden">Included when self-hosting</span>

                                </td>
                                <td><span className="hidden">Included when self-hosting</span>

                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (three ? "" : "none") }}>
                                <td className="subtitulo_tabla">Elaboración de CFDI's por pago de dividendos</td>
                                <td><span className="hidden">Included when self-hosting</span>

                                </td>
                                <td><span className="hidden">Included when self-hosting</span>

                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (three ? "" : "none") }}>
                                <td className="subtitulo_tabla">Asesoría. Ley Antilavado.
                                </td>
                                <td><span className="hidden">Included when self-hosting</span>

                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (three ? "" : "none") }}>
                                <td className="subtitulo_tabla">Presentación de la declaración de operaciones relevantes
                                </td>
                                <td><span className="hidden">Included when self-hosting</span>

                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (three ? "" : "none") }}>
                                <td className="subtitulo_tabla">Estudios de precios de trasferencia. Aviso de cuando cumplir.
                                </td>
                                <td><span className="hidden">Included when self-hosting</span>

                                </td>
                                <td><span className="hidden">Included when self-hosting</span>

                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (three ? "" : "none") }}>
                                <td className="subtitulo_tabla">Control de requerimientos fiscales adicionales de algunas
                                    deducciones
                                </td>
                                <td><span className="hidden">Included when self-hosting</span>

                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (three ? "" : "none") }}>
                                <td className="subtitulo_tabla">Control de obligaciones. Ley general de sociedades mercantiles
                                </td>
                                <td><span className="hidden">Included when self-hosting</span>

                                </td>
                                <td><span className="hidden">Included when self-hosting</span>

                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (three ? "" : "none") }}>
                                <td className="subtitulo_tabla">Control de obligaciones. Por manejar inversión extrajera
                                </td>
                                <td><span className="hidden">Included when self-hosting</span>

                                </td>
                                <td><span className="hidden">Included when self-hosting</span>

                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (three ? "" : "none") }}>
                                <td className="subtitulo_tabla">Control de obligaciones. Por tener socios extranjeros
                                </td>
                                <td><span className="hidden">Included when self-hosting</span>

                                </td>
                                <td><span className="hidden">Included when self-hosting</span>

                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (three ? "" : "none") }}>
                                <td className="subtitulo_tabla">Control de obligaciones. Por tener trabajadores extranjeros
                                </td>
                                <td><span className="hidden">Included when self-hosting</span>

                                </td>
                                <td><span className="hidden">Included when self-hosting</span>

                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (three ? "" : "none") }}>
                                <td className="subtitulo_tabla">Control de obligaciones. Otra obligacion de leyes especiales
                                </td>
                                <td><span className="hidden">Included when self-hosting</span>

                                </td>
                                <td><span className="hidden">Included when self-hosting</span>

                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr className="gh-pricing-category" onClick={() => setFour(!four)} style={{cursor: 'pointer'}}>
                                <td colSpan="4">Buzón tributario</td>
                                <td style={{border: 0}}>
                                    <img src="https://img.icons8.com/material-sharp/24/000000/sort-down.png" style={{width: 15, height: 10}}/>
                                </td>
                            </tr>
                            <tr style={{ display: (four ? "" : "none") }}>
                                <td className="subtitulo_tabla">Seguimiento semanal del buzón tributario en la pagina del SAT
                                </td>
                                <td><span className="hidden">Included when self-hosting</span>

                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr className="gh-pricing-category" onClick={() => setFive(!five)} style={{cursor: 'pointer'}}>
                                <td colSpan="4">Estado financiero e informes financieros</td>
                                <td style={{border: 0}}>
                                    <img src="https://img.icons8.com/material-sharp/24/000000/sort-down.png" style={{width: 15, height: 10}}/>
                                </td>
                            </tr>
                            <tr style={{ display: (five ? "" : "none") }}>
                                <td className="subtitulo_tabla">Estados financieros mensuales. Español y/o ingles</td>
                                <td><span className="hidden">Included when self-hosting</span>

                                </td>
                                <td><span className="hidden">Included when self-hosting</span>

                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (five ? "" : "none") }}>
                                <td className="subtitulo_tabla">Principales indicadores del negocio en graficas</td>
                                <td><span className="hidden">Included when self-hosting</span>

                                </td>
                                <td><span className="hidden">Included when self-hosting</span>

                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (five ? "" : "none") }}>
                                <td className="subtitulo_tabla">Informes financieros en ONbusiness disponible 365 días
                                    del año</td>
                                <td><span className="hidden">Included when self-hosting</span>

                                </td>
                                <td><span className="hidden">Included when self-hosting</span>

                                </td>
                                <td><span className="hidden">Included when self-hosting</span>

                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (five ? "" : "none") }}>
                                <td className="subtitulo_tabla">Manual de administración</td>
                                <td><span className="hidden">Included when self-hosting</span>

                                </td>
                                <td><span className="hidden">Included when self-hosting</span>

                                </td>
                                <td><span className="hidden">Included when self-hosting</span>

                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr className="gh-pricing-category" onClick={() => setSix(!six)} style={{cursor: 'pointer'}}>
                                <td colSpan="4">Calculo de la nómina</td>
                                <td style={{border: 0}}>
                                    <img src="https://img.icons8.com/material-sharp/24/000000/sort-down.png" style={{width: 15, height: 10}}/>
                                </td>
                            </tr>
                            <tr style={{ display: (six ? "" : "none") }}>
                                <td className="subtitulo_tabla">Administración del sistema de nóminas
                                    para todos los trabajadores</td>
                                <td><span className="hidden">Included when self-hosting</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (six ? "" : "none") }}>
                                <td className="subtitulo_tabla">Cálculo de la nómina de los trabajadores</td>
                                <td><span className="hidden">Included when self-hosting</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (six ? "" : "none") }}>
                                <td className="subtitulo_tabla">Timbrado de la nómina y envío de recibos a los trabajadores</td>
                                <td><span className="hidden">Included when self-hosting</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (six ? "" : "none") }}>
                                <td className="subtitulo_tabla">Cálculo de finiquitos y liquidaciones</td>
                                <td><span className="hidden">Included when self-hosting</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (six ? "" : "none") }}>
                                <td className="subtitulo_tabla">Asesoría fiscal basica por renuncias, abandonos de empleo,
                                despidos justificados y despidos injustificados</td>
                                <td><span className="hidden">Included when self-hosting</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr className="gh-pricing-category" onClick={() => setSeven(!seven)} style={{cursor: 'pointer'}}>
                                <td colSpan="4">Calculo de IMSS</td>
                                <td style={{border: 0}}>
                                    <img src="https://img.icons8.com/material-sharp/24/000000/sort-down.png" style={{width: 15, height: 10}}/>
                                </td>
                            </tr>
                            <tr style={{ display: (seven ? "" : "none") }}>
                                <td className="subtitulo_tabla">Administración del SUA y contro de IDSE para todos los trabajadores</td>
                                <td><span className="hidden">Included when self-hosting</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (seven ? "" : "none") }}>
                                <td className="subtitulo_tabla">Càlculo y control del salario diario integrado de los trabajadores</td>
                                <td><span className="hidden">Included when self-hosting</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (seven ? "" : "none") }}>
                                <td className="subtitulo_tabla">Movimientos de altas, bajas y modificaciones en IDSE y SUA</td>
                                <td><span className="hidden">Included when self-hosting</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (seven ? "" : "none") }}>
                                <td className="subtitulo_tabla">Càlculo del IMSS y envío de archivos para pago</td>
                                <td><span className="hidden">Included when self-hosting</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr className="gh-pricing-category" onClick={() => setEight(!eight)} style={{cursor: 'pointer'}}>
                                <td colSpan="4">Calculo del impuesto sobre nóminas</td>
                                <td style={{border: 0}}>
                                    <img src="https://img.icons8.com/material-sharp/24/000000/sort-down.png" style={{width: 15, height: 10}}/>
                                </td>
                            </tr>
                            <tr style={{ display: (eight ? "" : "none") }}>
                                <td className="subtitulo_tabla">Administración del sistema y papeles de trabajo del ISN</td>
                                <td><span className="hidden">Included when self-hosting</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (eight ? "" : "none") }}>
                                <td className="subtitulo_tabla">Càlculo de impuesto sobre nóminas y envio de archivos para pago</td>
                                <td><span className="hidden">Included when self-hosting</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <svg data-cy="check-icon" viewBox="0 0 24 24">
                                            <path d="M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z"></path>
                                        </svg>
                                    </div>
                                </td>
                            </tr>
                            <tr className="gh-pricing-category" onClick={() => setNine(!nine)} style={{cursor: 'pointer'}}>
                                <td colSpan="4">Calculo del impuesto sobre nóminas</td>
                                <td style={{border: 0}}>
                                    <img src="https://img.icons8.com/material-sharp/24/000000/sort-down.png" style={{width: 15, height: 10}}/>
                                </td>
                            </tr>
                            <tr style={{ display: (nine ? "" : "none") }}>
                                <td className="subtitulo_tabla">Asesoria fiscal para las operaciones diarias de la empresa</td>
                                <td><span className="hidden">Included when self-hosting</span>
                                    <div className="gh-pricing-check">
                                        <p style={{fontSize: 'medium', color: '#96c02d'}}>Incluido sin limite</p>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <p style={{fontSize: 'medium', color: '#96c02d'}}>Incluido sin limite</p>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <p style={{fontSize: 'medium', color: '#96c02d'}}>Incluido sin limite</p>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <p style={{fontSize: 'medium', color: '#96c02d'}}>Incluido sin limite</p>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (nine ? "" : "none") }}>
                                <td className="subtitulo_tabla">Asesoria fiscal para planear u optimizar los resultados de la empresa</td>
                                <td><span className="hidden">Included when self-hosting</span>
                                    <div className="gh-pricing-check">
                                        <p style={{fontSize: 'medium', color: '#96c02d'}}>No</p>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <p style={{fontSize: 'medium', color: '#96c02d'}}>No</p>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <p style={{fontSize: 'medium', color: '#96c02d'}}>1 anual</p>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <p style={{fontSize: 'medium', color: '#96c02d'}}>2 anuales</p>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (nine ? "" : "none") }}>
                                <td className="subtitulo_tabla">Asesoria legal en materia civil, mercantil, laboral y comportativa</td>
                                <td><span className="hidden">Included when self-hosting</span>
                                    <div className="gh-pricing-check">
                                        <p style={{fontSize: 'medium', color: '#96c02d'}}>No</p>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <p style={{fontSize: 'medium', color: '#96c02d'}}>1 al mes</p>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <p style={{fontSize: 'medium', color: '#96c02d'}}>2 al mes</p>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <p style={{fontSize: 'medium', color: '#96c02d'}}>Incluido sin limite</p>
                                    </div>
                                </td>
                            </tr>
                            <tr style={{ display: (nine ? "" : "none") }}>
                                <td className="subtitulo_tabla">Asesoria en administración de negocios</td>
                                <td><span className="hidden">Included when self-hosting</span>
                                    <div className="gh-pricing-check">
                                        <p style={{fontSize: 'medium', color: '#96c02d'}}>No</p>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in basic plan</span>
                                    <div className="gh-pricing-check">
                                        <p style={{fontSize: 'medium', color: '#96c02d'}}>No</p>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in standard plan</span>
                                    <div className="gh-pricing-check">
                                        <p style={{fontSize: 'medium', color: '#96c02d'}}>No</p>
                                    </div>
                                </td>
                                <td><span className="hidden">Included in business plan</span>
                                    <div className="gh-pricing-check">
                                        <p style={{fontSize: 'medium', color: '#96c02d'}}>Incluido sin limite</p>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </section>
                </div>
            </div>
        </div>
    )
}

export default PricingTable;